﻿using HttpExhaustiver.entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace HttpExhaustiver.handle
{
    class DicQueue
    {
        private Dictionary<string, ExhausitiverDic> PARAM_WITH_PATH = new Dictionary<string, ExhausitiverDic>();

        private BlockingCollection<Dictionary<string, string>> queue = new BlockingCollection<Dictionary<string, string>>();


        public void Destroy()
        {
            foreach (var line in PARAM_WITH_PATH)
            {
                try
                {
                    line.Value.Reader.Close();
                }
                catch { }
            }
        }
        public DicQueue(Dictionary<string, string> paramWithPath)
        {

            foreach (var line in paramWithPath)
            {
                Num = Num * GetLine(line.Value);
            }
            if (Num == 0)
            {
                return;
            }
            foreach (var line in paramWithPath)
            {
                ExhausitiverDic dic = new ExhausitiverDic();
                dic.Param = line.Key;
                dic.Path = line.Value;
                dic.Reader = new StreamReader(dic.Path);
                PARAM_WITH_PATH.Add(dic.Param, dic);
            }
            Task.Factory.StartNew(() =>
            {
                ///初始化第一条字典
                Dictionary<string, string> dic = new Dictionary<string, string>();
                foreach (var line in PARAM_WITH_PATH)
                {
                    line.Value.Current = line.Value.Reader.ReadLine();
                    dic.Add(line.Key, line.Value.Current);
                }
                queue.Add(dic);
                ///遍历字典合集
                int index = PARAM_WITH_PATH.Count - 1;
                while (index > -1)
                {
                    if (queue.Count > 20000)
                    {
                        Thread.Sleep(10);
                        continue;
                    }
                    dic = new Dictionary<string, string>();
                    for (int i = 0; i < PARAM_WITH_PATH.Count; i++)
                    {
                        var element = PARAM_WITH_PATH.ElementAt(i);
                        if (index == i)
                        {
                            string value = element.Value.Reader.ReadLine();
                            if (value == null)
                            {
                                ///文件读取完毕
                                index--;
                                element.Value.Reader.BaseStream.Seek(0, SeekOrigin.Begin);
                                break;
                            }
                            element.Value.Current = value;
                            if (index != PARAM_WITH_PATH.Count - 1)
                            {
                                index++;
                                break;
                            }
                            dic.Add(element.Key, element.Value.Current);
                            continue;
                        }
                        dic.Add(element.Key, element.Value.Current);
                    }
                    if (dic.Count != PARAM_WITH_PATH.Count)
                    {
                        continue;
                    }
                    queue.Add(dic);
                }
                Finished = true;
            }, TaskCreationOptions.LongRunning);
        }

        public bool Finished { get; set; } = false;
        public long Num { get; set; } = 1;

        private static long GetLine(string filePath)
        {
            long line = 0;
            StreamReader sr = new StreamReader(filePath);
            while (!sr.EndOfStream)
            {
                sr.ReadLine();
                line++;
            }
            sr.Close();
            return line;
        }
        public Dictionary<String, String> Next()
        {
            Dictionary<string, string> item;
            queue.TryTake(out item, TimeSpan.FromSeconds(5));
            return item;
        }

    }
}
